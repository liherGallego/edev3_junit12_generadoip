package jUnit12_GeneradoIP;

import java.util.Random;
import java.util.stream.IntStream;

/**
 * 
 * @author liher
 */
public class GeneradorIp {
	
		public GeneradorIp() {
			
		}
		//devuelve un n�mero entre min y max
		public int generarNumero(int min, int max) {
			Random rdn = new Random();
			int num;
			num = rdn.nextInt((max - min) + 1) + min;
			return num;
		}
			
		//devuelve una direcci�n ip, utilizando el m�todo anterior.	
		public String generarIp() {
			
			String ip=""; 
			for (int i=0; i<4;i++) {
				int num = generarNumero(0,254);
				if (i<3) {	
					ip += num + "."; 
					}
				ip += num;
			}
			return ip;
		}	
	}


