package jUnit12_GeneradoIPTest;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import jUnit12_GeneradoIP.GeneradorIp;

class GeneradorIpTest3 {
	GeneradorIp generador;
	@BeforeEach
	void setUp() throws Exception {
		generador = new GeneradorIp();
	}
	
	@Test
	void test() {
		for (int i=0; i<10;i++) {
		int num =  generador.generarNumero(0, 254);
		assertTrue(num>=0 && num<=254);
		}
		
		String ip = generador.generarIp();
		String[]numeros = ip.split("\\.");
		assertTrue(numeros[0] != "0" && numeros[numeros.length-1] != "0");
		}
}
